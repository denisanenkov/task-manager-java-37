<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK LIST</h1>

<table width="100%" border="1" cellpadding="5" style="border-collapse: collapse;">

    <tr>
        <th width="100" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="100%">DESCRIPTION</th>
        <th width="150" nowrap="nowrap" align="center">STATUS</th>
        <th width="150" nowrap="nowrap" align="center">PROJECT</th>
        <th width="130" nowrap="nowrap" align="center">DATE BEGIN</th>
        <th width="130" nowrap="nowrap" align="center">DATE FINISH</th>
        <th width="70" nowrap="nowrap" align="center">EDIT</th>
        <th width="70" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach items="${tasks}" var="task">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td align="center">
                <c:out value="${task.status.displayName}"/>
            </td>
            <td align="center">
                <c:out value="${task.project.name}"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${task.dateBegin}" type="date" pattern="dd-MM-yyyy"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${task.dateFinish}" type="date" pattern="dd-MM-yyyy"/>
            </td>
            <td align="center">
                <a href="/task/edit/${task.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/task/delete/${task.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>

</table>
<br>
<br>
<form action="/task/create">
    <button>
        CREATE TASK
    </button>
</form>
<br>
<form action="/">
    <button>
        START PAGE
    </button>
</form>
<jsp:include page="../include/_footer.jsp"/>
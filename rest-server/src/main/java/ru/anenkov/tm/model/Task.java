package ru.anenkov.tm.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "task")
public class Task {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name = "";

    @Column
    private String description = "";

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @ManyToOne
    @JsonBackReference
    private Project project;

    public Task(String name) {
        this.name = name;
    }

    public Task(Project project) {
        this.project = project;
    }

}

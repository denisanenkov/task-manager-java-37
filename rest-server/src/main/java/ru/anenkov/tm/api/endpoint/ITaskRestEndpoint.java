package ru.anenkov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.model.Task;

import java.util.List;

@RequestMapping("/api")
public interface ITaskRestEndpoint {

    @GetMapping("/tasks")
    List<Task> tasks();

    @GetMapping("/task/{id}")
    Task getTask(@PathVariable String id);

    @PostMapping("/task")
    Task addTask(@RequestBody Task task);

    @PutMapping("/task")
    Task updateTask(@RequestBody Task task);

    @DeleteMapping("/task/{id}")
    String deleteTask(@PathVariable String id);

    @DeleteMapping("/tasks")
    void deleteAllTasks();

    @GetMapping("/tasks/count")
    long count();

}

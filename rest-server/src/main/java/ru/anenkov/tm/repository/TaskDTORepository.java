package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.anenkov.tm.dto.TaskDTO;

import java.util.List;

public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String projectId);

    @Nullable
    TaskDTO findEntityByIdAndProjectId(@Nullable String id, @Nullable String projectId);

    void removeByProjectIdAndId(@NotNull final String projectId, @NotNull final String id);

    void removeAllByProjectId(@NotNull final String projectId);

}

package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.anenkov.tm.dto.ProjectDTO;

public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {


}

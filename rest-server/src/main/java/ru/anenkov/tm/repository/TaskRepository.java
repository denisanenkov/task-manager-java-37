package ru.anenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;

import java.util.*;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    List<Task> findAllByProjectId(@Nullable String projectId);

    @Nullable
    Task findEntityByIdAndProjectId(@Nullable String id, @Nullable String projectId);

    void removeByProjectIdAndId(@NotNull final String projectId, @NotNull final String id);

    void removeAllByProjectId(@NotNull final String projectId);

}
